A client might want to utilize a private repo for a number of reasons. The first being that the software being produced is not open source.
If the it is open source maybe its not ready to be released to the public. It also makes version control much easier; fi a problem happens that breaks the functionality of the code
you can simply revert to an older version of the code where everything was working perfectly.
Havign a private repo also makes tracking of daily progress much easier, with a visual representation.
Sharing code amongst developers is much easier with a repo and in the case a developer needs to be replaced, simply give the new developer access to the repo and they are ready to go.

License Source:
https://opensource.org/licenses/APSL-2.0