﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace NinjaPopper
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        private StartScene startScene;
        private ActionScene actionScene;
        private HelpScene helpScene;
        private AboutScene aboutScene;
        

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.IsFullScreen = true;
            graphics.HardwareModeSwitch = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Shared.stage = new Vector2(graphics.PreferredBackBufferWidth,
                graphics.PreferredBackBufferHeight);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            startScene = new StartScene(this);
            actionScene = new ActionScene(this);
            helpScene = new HelpScene(this);
            aboutScene = new AboutScene(this);

            this.Components.Add(startScene);
            this.Components.Add(actionScene);
            this.Components.Add(helpScene);
            this.Components.Add(aboutScene);
            startScene.Show();

            Song backgroundMusic = this.Content.Load<Song>("music/background");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(backgroundMusic);



            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (CollisionManager.gameOver == true)
            {
                
                for (int i = 0; i < actionScene.componenets.Count; i++)
                {
                    if (actionScene.componenets[i] is Enemy)
                    {
                        Enemy enemyFound = (Enemy)actionScene.componenets[i];
                        enemyFound.Enabled = false;
                        enemyFound.startAnimation = false;
                        enemyFound.frameIndex = 0;
                    }

                    if (actionScene.componenets[i] is CollisionManager)
                    {
                        actionScene.componenets[i].Enabled = false;
                    }
                }
                
            }
            int selectedIndex = 0;
            KeyboardState ks = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            if (ActionScene.enemiesKilled == 5)
            {
                Enemy.speed.Y += 0.5f;
                ActionScene.enemiesKilled = 0;
            }
            

            if (startScene.Enabled)
            {
                selectedIndex = startScene.Menu.SelectedIndex;
                if (selectedIndex == 0 && ks.IsKeyDown(Keys.Enter))
                {
                    startScene.Hide();
                    actionScene.Show();
                }
                if (selectedIndex == 1 && ks.IsKeyDown(Keys.Enter))
                {
                    startScene.Hide();
                    helpScene.Show();
                }
                if (selectedIndex == 2 && ks.IsKeyDown(Keys.Enter))
                {
                    startScene.Hide();
                    aboutScene.Show();
                }
                if (selectedIndex == 3 && ks.IsKeyDown(Keys.Enter))
                {
                    Exit();
                } 
            }

            if (ks.IsKeyDown(Keys.Escape) && actionScene.Enabled == true)
            {
                if (CollisionManager.gameOver == false)
                {
                    for (int i = 0; i < actionScene.componenets.Count; i++)
                    {
                        if (actionScene.componenets[i] is Enemy)
                        {
                            Enemy enemyFound = (Enemy)actionScene.componenets[i];
                            enemyFound.Enabled = false;
                            enemyFound.startAnimation = false;
                            enemyFound.frameIndex = 0;
                        }
                    }
                }
                ActionScene.enemiesModifier = 0;
                ActionScene.points = 0;
                Enemy.speed.Y = 2;
                CollisionManager.gameOver = false;
                actionScene.Hide();
                startScene.Show();
            }

            if (ks.IsKeyDown(Keys.Escape) && helpScene.Enabled == true)
            {
                helpScene.Hide();
                startScene.Show();
            }

            if (ks.IsKeyDown(Keys.Escape) && aboutScene.Enabled == true)
            {
                aboutScene.Hide();
                startScene.Show();
            }

            if (ms.Y >= Shared.stage.Y)
            {
                Mouse.SetPosition(ms.X, (int)Shared.stage.Y);
            }

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gray);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
