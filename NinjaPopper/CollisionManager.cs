﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace NinjaPopper
{
    public class CollisionManager : GameComponent
    {
        private Enemy enemy;
        private CrossHair crossHair;
        private PointsCounter points;
        public bool hitMarker;
        MouseState previousState;
        public static bool gameOver = false;
        public CollisionManager(Game game, CrossHair crossHair, Enemy enemy, PointsCounter points) : base(game)
        {
            this.crossHair = crossHair;
            this.enemy = enemy;
            this.previousState = Mouse.GetState();
            this.points = points;
        }

        public override void Update(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();
            Rectangle enemyRect = crossHair.getBound();
            Rectangle crossHairRect = enemy.getBound();

            if (enemyRect.Intersects(crossHairRect) && ms.LeftButton == ButtonState.Pressed && previousState.LeftButton != ButtonState.Pressed
                && enemy.startAnimation == false)
            {
                enemy.start();
                ActionScene.enemiesKilled++;
                ActionScene.points += 2.8;
                
            }
            else if (!enemyRect.Intersects(crossHairRect) && ms.LeftButton == ButtonState.Pressed && previousState.LeftButton != ButtonState.Pressed)
            {
                ActionScene.points -= 0.2;
            }
            points.message = "Points: " + ActionScene.points;


            base.Update(gameTime);
            previousState = ms;
        }
    }
}
