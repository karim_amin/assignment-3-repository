﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace NinjaPopper
{
    public class CrossHair : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D texture;
        private Vector2 position;
        public bool hitMarker;


        //private Vector2 speed;
        public CrossHair(Game game, SpriteBatch spriteBatch,
         Texture2D texture) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.texture = texture;

            
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(texture, position, Color.White);
            spriteBatch.End();
            
            
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            MouseState x = Mouse.GetState();
            position = x.Position.ToVector2();
            if (position.X > Shared.stage.X - texture.Width)
            {
                position.X = Shared.stage.X - texture.Width;
            }
            if (position.X < 0)
            {
                position.X = 0;
            }

            if (position.Y > Shared.stage.Y - texture.Height)
            {
                position.Y = Shared.stage.Y - texture.Height;
            }
            if (position.Y < 0)
            {
                position.Y = 0;
            }
            base.Update(gameTime);
        }
        public Rectangle getBound()
        {
            return new Rectangle((int)position.X + texture.Width / 2, (int)position.Y + texture.Height / 2, 1, 1);
        }
    }
}
