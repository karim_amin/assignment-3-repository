﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;    

namespace NinjaPopper
{
    
    public abstract class GameScene : DrawableGameComponent
    {
        public List<GameComponent> componenets;

        public List<GameComponent> Componenets { get => componenets; set => componenets = value; }

        public virtual void Show()
        {
            this.Enabled = true;
            this.Visible = true;
        }
        public virtual void Hide()
        {
            this.Enabled = false;
            this.Visible = false;
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var item in componenets)
            {
                if (item.Enabled)
                {
                    item.Update(gameTime);
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawableGameComponent comp = null;
            foreach (var item in componenets)
            {
                if (item is DrawableGameComponent)
                {
                    comp = (DrawableGameComponent)item;
                    if (comp.Visible)
                    {
                        comp.Draw(gameTime);
                    }
                }
            }
            base.Draw(gameTime);
        }

        public GameScene(Game game) : base(game)
        {
            componenets = new List<GameComponent>();
            this.Hide();
        }
    }
}
