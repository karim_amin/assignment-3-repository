﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace NinjaPopper
{
    public class Enemy : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D texture;
        private Vector2 position;
        private Vector2 size;
        private List<Rectangle> frames;
        public int frameIndex = 0;
        private int delay;
        private int delayCounter;
        const int SPRITE_SHEET_WIDTH_CORRECTION = 2;
        public bool startAnimation;
        public static Vector2 speed;

        private const int ROW = 7;
        private const int COL = 1;

        public Vector2 Position { get => position; set => position = value; }

        public void start()
        {
            this.startAnimation = true;
            
        }

        public void stop()
        {
            this.startAnimation = false;
            position = new Vector2(position.X, -texture.Height);

        }

        public Enemy(Game game,
            SpriteBatch spriteBatch,
            Texture2D texture,
            Vector2 position,
            int delay
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.texture = texture;
            this.position = position;
            this.delay = delay;
            speed = new Vector2(0, 2);
            size = new Vector2(texture.Width / ROW + SPRITE_SHEET_WIDTH_CORRECTION, texture.Height / COL);
            this.Visible = true;
            this.Enabled = true;

            //Make the frames here
            createFrames();

        }

        private void createFrames()
        {
            frames = new List<Rectangle>();
            for (int i = 0; i < ROW; i++)
            {
                for (int j = 0; j < COL; j++)
                {
                    frames.Add(new Rectangle(i * (int)size.X, j * (int)size.Y, (int)size.X, (int)size.Y));
                }
            }

        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            if (frameIndex > -1)
            {
                spriteBatch.Draw(texture, position, frames[frameIndex], Color.White);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {

            
            position += speed;

            

            if (position.Y >= Shared.stage.Y && this.startAnimation == false)
            {
                CollisionManager.gameOver = true;
            }

            if (startAnimation == true)
            {
                delayCounter++;
                if (delayCounter > delay)
                {
                    frameIndex++;
                    if (frameIndex > ROW * COL - 1)
                    {
                        stop();
                        frameIndex = 0;
                    }
                    delayCounter = 0;
                }
            }

            base.Update(gameTime);
        }
        public Rectangle getBound()
        {
            return new Rectangle((int)position.X, (int)position.Y, texture.Width / ROW, texture.Height / COL);
        }
    }
}
