﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace NinjaPopper
{
    public class StartScene : GameScene
    {
        private MenuComponent menu;
        private SpriteBatch spriteBatch;
        private string[] menuItems = { "Start Practicing", "Help", "Credits", "Quit" };

        public StartScene(Game game) : base(game)
        {

            Game1 g = (Game1)game;
            g.spriteBatch = new SpriteBatch(GraphicsDevice);
            SpriteFont regularFont = g.Content.Load<SpriteFont>("fonts/regularFont");
            SpriteFont hilightedFont = g.Content.Load<SpriteFont>("fonts/hilightedFont");
            menu = new MenuComponent(g, spriteBatch, regularFont, hilightedFont, menuItems);
            this.componenets.Add(menu);
        }

        public MenuComponent Menu { get => menu; set => menu = value; }
    }
}
