﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace NinjaPopper
{
    public class ActionScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private CrossHair crossHair;
        private Enemy enemy;
        public CollisionManager collisionManager;
        const int DELAY = 3;
        private const int NUMBER_OF_STARTING_ENEMIES = 5;
        public static int enemiesModifier = 0;
        public static int totalEnemies;
        private Vector2 position;
        public static double points = 0;
        Game1 g;
        public static int enemiesKilled = 0;


        public ActionScene(Game game) : base(game)
        {
            
            totalEnemies = NUMBER_OF_STARTING_ENEMIES + enemiesModifier;
            position = new Vector2(70, 0);
            g = (Game1)game;
            SpriteFont regularFont = g.Content.Load<SpriteFont>("fonts/regularFont");
            this.spriteBatch = g.spriteBatch;
            Texture2D texture = g.Content.Load<Texture2D>("sprites/Crosshair");
            crossHair = new CrossHair(g, spriteBatch, texture);
            Texture2D enemyTexture = g.Content.Load<Texture2D>("sprites/balloon");

            PointsCounter pc = new PointsCounter(g, spriteBatch, regularFont, Vector2.Zero, "Points: " + points , Color.Red);
            this.componenets.Add(pc);

            for (int i = 0; i < totalEnemies; i++)
            {
                enemy = new Enemy(g, spriteBatch, enemyTexture, position, DELAY);
                this.componenets.Add(enemy);
                collisionManager = new CollisionManager(g, crossHair, enemy, pc);
                this.componenets.Add(collisionManager);
                position += new Vector2(150, 0);
            }
            this.componenets.Add(crossHair);
            
        }

        public override void Show()
        {
            collisionManager.Enabled = true;
            for (int i = 0; i < Componenets.Count(); i++)
            {
                
                if (Componenets[i] is Enemy)
                {
                    Enemy x = (Enemy)Componenets[i];
                    x.Position = new Vector2(x.Position.X, 0);
                    x.Visible = true;
                    x.Enabled = true;
                }

                if (Componenets[i] is CollisionManager)
                {
                    Componenets[i].Enabled = true;
                }
            }
            
            base.Show();
        }




    }
}
